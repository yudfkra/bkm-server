<div class="row">
    <div class="col-md-12">        
        <div class="panel no-border panel-dark">
            <div class="panel-body text-center">
                <h3 class="text-lg text-slim text-warning">
                    <i class="fa fa-warning text-yellow"></i>
                    {{ trans_choice('messages.errors.not_found', !empty($item), ['item' => $item ?? '']) }}
                </h3>
                <p style="margin: 20px 0 20px 0;">
                    {!! trans('layout.notfound.back_or_search', [
                        'url' => url()->previous() != url()->current() ? url()->previous() : $route,
                    ]) !!}
                </p>
                {{ Form::open([
                    'url' => $route,
                    'method' => "GET",
                    'class' => 'form-inline',
                ]) }}
                    <div class="form-group">
                        <label class="sr-only" for="input-keyword">@lang('common.field.keyword')</label>
                        <input type="text" name="keyword" class="form-control" id="input-keyword" placeholder="@lang('common.field.keyword')">
                    </div>
                    <button type="submit" class="btn btn-primary">{{ trans_choice('common.buttons.search', !empty($item), ['item' => $item ?? '']) }}</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>