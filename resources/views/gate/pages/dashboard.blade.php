@extends('gate.layouts.app')
@section('content')
    @component('gate.layouts.page_header')
        @slot('title') {{ $title }} @endslot
    @endcomponent
    <div class="row">
        @foreach ($brokers as $broker)
            <div class="col-md-6">
                <div class="stat-panel">
                    <div class="stat-cell bg-danger valign-middle">
                        <i class="fa fa-truck bg-icon"></i>
                        <span class="text-xlg"><strong>{{ $broker->name }}</strong></span><br>
                        <span class="text-sm"><a href="{{ $broker->url }}">{{ trans('layout.menu.dashboard.to_app') }}</a></span>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection