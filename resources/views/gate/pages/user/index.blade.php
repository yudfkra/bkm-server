@extends('gate.layouts.app')
@section('content')
    @includeWhen(session('flash_messages'), 'gate.layouts.feedback')
    @component('gate.layouts.page_header')
        @slot('title') {{ $title }} @endslot
        <div class="pull-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#user-modal">
                <i class="fa fa-plus"></i> @lang('page/user.section.add')
            </button>
        </div>
    @endcomponent
    <div class="panel-group" id="filter-group">
        <div class="panel">
            <div class="panel-heading">
                <a class="accordion-toggle {{ !empty($params) ? 'collapsed' : 'collapse' }}" data-toggle="collapse" href="#collapseFilter" data-parent="#filter-group">@lang('page/user.section.filter')</a>
            </div>
            <div id="collapseFilter" class="panel-collapse collapse {{ !empty($params) ? 'in' : '' }}">
                <div class="panel-body">
                    {{ Form::open(['route' => 'gate.user.index', 'method' => 'GET']) }}
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label for="filter-type" class="control-label">@lang('page/user.field.user_type')</label>
                                {{ Form::select('type', $user_types, $params['type'] ?? null, [
                                    'placeholder' => trans('page/user.field.user_type'),
                                    'class' => 'form-control select2',
                                    'id' => 'filter-type'
                                ]) }}
                            </div>
                            <div class="form-group col-md-3">
                                <label for="filter-status" class="control-label">@lang('page/user.field.status.title')</label>
                                {{ Form::select('status', $user_status, $params['status'] ?? null, [
                                    'placeholder' => trans('page/user.field.status.title'),
                                    'class' => 'form-control select2',
                                    'id' => 'filter-status'
                                ]) }}
                            </div>
                            <div class="form-group col-md-4">
                                <label for="filter-keyword" class="control-label">@lang('common.field.keyword')</label>
                                <input type="text" name="keyword" id="filter-keyword" class="form-control" value="{{ $params['keyword'] ?? null }}" placeholder="{{ trans('common.field.keyword') }}" autocomplete="off">
                            </div>
                            <div class="form-group col-md-2">
                                <button type="submit" class="btn btn-primary col-md-12" style="margin-top: 22px;">{{ trans_choice('common.buttons.search', 0) }}</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">@lang('layout.menu.user.description')</span>
        </div>
        <div class="panel-body with-margins">
            <div class="table-primary table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <th>@lang('page/user.field.name')</th>
                            <th>@lang('page/user.field.email')</th>
                            <th style="width: 140px;">@lang('page/user.field.user_type')</th>
                            <th style="width: 100px;">@lang('page/user.field.status.title')</th>
                            <th style="width: 160px;">@lang('page/user.field.created_at')</th>
                            <th style="width: 100px;">@lang('common.text.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($users as $user)
                            <tr>
                                <td>{{ $offset++ }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td class="text-left">
                                    <span class="label label-{{ $user->type->label_type_display }}">{{ $user->type->type }}</span>
                                </td>
                                <td class="text-center">
                                    <span class="label label-{{ $user->status ? 'success' : 'danger' }}">{{ trans('page/user.field.status.' . $user->user_status_display) }}</span>
                                </td>
                                <td>{{ $user->created_at ? $user->created_at->format('H:i - d F Y') : '-' }}</td>
                                <td>
                                    <a href="{{ route('gate.user.show', ['user' => $user->id]) }}" class="btn btn-primary" data-toggle="tooltip" title="{{ trans_choice('common.buttons.detail', 0) }}" data-placement="top">
                                        <i class="fa fa-search"></i>
                                    </a>
                                    {{-- <a href="{{ route('gate.user.edit', ['user' => $user->id]) }}" class="btn btn-success" data-toggle="tooltip" title="{{ trans_choice('common.buttons.edit', 0) }}" data-placement="top">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a> --}}
                                    {{ Form::open([
                                        'method'=>'delete',
                                        'route' => ['gate.user.destroy', $user->id],
                                        'style' => 'display:inline;'
                                    ]) }}
                                        <button type="button" class="btn btn-danger confirm-modal"><i class="fa fa-trash-o" data-toggle="tooltip" title="{{ trans_choice('common.buttons.delete', 0) }}" data-placement="top"></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7" align="center">{{ trans('messages.errors.empty_data') }}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="pull-right">
                {!! $users->links() !!}
            </div>
        </div>
    </div>
@endsection
@push('html')
    @include('gate.pages.user.modal_user_form', [
        'route' => 'gate.user.store',
        'method' => 'POST',
        'modalTitle' => trans('page/user.section.add'),
        'userdata' => false,
    ])
@endpush