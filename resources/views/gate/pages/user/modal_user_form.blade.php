<div id="user-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="modalHeading">{{ $modalTitle }}</h4>
            </div>
            {{ Form::open(['route' => $route, 'method' => $method, 'class' => 'form-horizontal']) }}
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('name') ? 'has-error simple' : '' }}" data-id="input-name">
                        <label for="input-name" class="col-sm-2 control-label">@lang('page/user.field.name')</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" value="{{ old('name', $userdata ? $userdata->name : null) }}" class="form-control" id="input-name" placeholder="@lang('page/user.field.name')">
                            {!! $errors->has("name") ? '<p class="help-block" data-id="input-name">'.$errors->first('name').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('email') ? 'has-error simple' : '' }}" data-id="input-email">
                        <label for="input-email" class="col-sm-2 control-label">@lang('page/user.field.email')</label>
                        <div class="col-sm-8">
                            <input type="email" name="email" value="{{ old('email', $userdata ? $userdata->email : null) }}" class="form-control" id="input-email" placeholder="@lang('page/user.field.email')">
                            {!! $errors->has("email") ? '<p class="help-block" data-id="input-email">'.$errors->first('email').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error simple' : '' }}" data-id="input-password">
                        <label for="input-password" class="col-sm-2 control-label">@lang('page/user.field.password')</label>
                        <div class="col-sm-8">
                            <input type="password" name="password" class="form-control" id="input-password" placeholder="@lang($userdata ? 'page/user.text.empty_if_not_update' : 'page/user.field.password')">
                            {!! $errors->has("password") ? '<p class="help-block" data-id="input-password">'.$errors->first('password').'</p>' : "" !!}
                        </div>
                    </div>
                    @if ($userdata)
                        <div class="form-group">
                            <label for="input-password-confirmation" class="col-sm-2 control-label">@lang('page/user.field.password_confirmation')</label>
                            <div class="col-sm-8">
                                <input type="password" name="password_confirmation" class="form-control" id="input-password-confirmation" placeholder="@lang('page/user.field.password_confirmation')">
                            </div>
                        </div>
                    @endif
                    <div class="form-group {{ $errors->has('user_type_id') ? 'has-error simple' : '' }}" data-id="input-type">
                        <label for="input-type" class="col-sm-2 control-label">@lang('page/user.field.user_type')</label>
                        <div class="col-sm-8">
                            {{ Form::select('user_type_id', $user_types, old('user_type_id', $userdata ? $userdata->user_type_id : null), [
                                'placeholder' => trans('page/user.field.user_type'),
                                'class' => 'form-control select2', 'id' => 'input-type'
                            ]) }}
                            {!! $errors->has("user_type_id") ? '<p class="help-block" data-id="input-type">'.$errors->first('user_type_id').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('status') ? 'has-error simple' : '' }}" data-id="input-status">
                        <label for="input-status" class="col-sm-2 control-label">@lang('page/user.field.status.title')</label>
                        <div class="col-sm-8">
                            {{ Form::select('status', $user_status, old('status', $userdata ? $userdata->status : null), [
                                'placeholder' => trans('page/user.field.status.title'),
                                'class' => 'form-control select2', 'id' => 'input-status'
                            ]) }}
                            {!! $errors->has("status") ? '<p class="help-block" data-id="input-status">'.$errors->first('status').'</p>' : "" !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ trans_choice('common.buttons.cancel', 0) }}</button>
                    <button type="submit" class="btn btn-primary">@lang('common.buttons.submit')</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@if (count($errors) > 0)
    @push('scripts')
        <script type="text/javascript">
            window.initPixel.push(function () {
                $('#user-modal').modal('show'); 
            });
        </script>
    @endpush 
@endif