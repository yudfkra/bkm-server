@extends('gate.layouts.app')
@section('content')
    @includeWhen(session('flash_messages'), 'gate.layouts.feedback')
    @if (!empty($user))
        <div class="row">
            <div class="col-md-10 col-xs-offset-1">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">
                            {{ trans_choice('page/user.section.detail', 1, ['user' => $user->name]) }}
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="input-name" class="col-sm-2 control-label">@lang('page/user.field.name')</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $user->name }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-email" class="col-sm-2 control-label">@lang('page/user.field.email')</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $user->email }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-user_type" class="col-sm-2 control-label">@lang('page/user.field.user_type')</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <span class="label label-{{ $user->type->label_type_display }}">{{ $user->type->type }}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-status" class="col-sm-2 control-label">@lang('page/user.field.status.title')</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <span class="label label-{{ $user->status ? 'success' : 'danger' }}">{{ trans('page/user.field.status.' . $user->user_status_display) }}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-created" class="col-sm-2 control-label">@lang('page/user.field.created_at')</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $user->created_at ? $user->created_at->format('H:i - d F Y') : '-' }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-updated" class="col-sm-2 control-label">@lang('page/user.field.updated_at')</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $user->updated_at ? $user->updated_at->format('H:i - d F Y') : '-' }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{ url()->previous() != url()->current() ? url()->previous() : route('gate.user.index') }}" class="btn btn-default">@lang('common.buttons.back')</a>
                        <button class="btn btn-success pull-right" data-toggle="modal" data-target="#user-modal"><i class="fa fa-pencil-square-o"></i> @lang('page/user.section.edit')</button>
                    </div>
                </div>
            </div>
        </div>
    @else
        @include('gate.pages.notfound', [
            'route' => route('gate.user.index'),
            'item' => trans('page/user.title')
        ])
    @endif
@endsection
@push('html')
    @includeWhen($user, 'gate.pages.user.modal_user_form', [
        'route' => ['gate.user.update', $user ? $user->id : null],
        'method' => 'PATCH',
        'modalTitle' => trans('page/user.section.edit'),
        'userdata' => $user,
    ])
@endpush