@extends('gate.layouts.auth')
@section('content')
    <h1 class="form-header">@lang('layout.menu.login.description')</h1>
    {!! Form::open([
        'url' => route('server.login', $params),
        'method'=>'post',
        'id'=>'signin-form',
        'class'=>'panel'
    ]) !!}
        @includeWhen(session('flash_messages'), 'gate.layouts.feedback')
        <div class="form-group {{ $errors->has('email') ? 'has-error simple' : '' }}">
            <input type="text" name="email" id="input-email" class="form-control input-lg" placeholder="{{ trans('page/user.field.email') }}" value="{{ old('email') }}">
            {!! $errors->has("email") ? '<p class="help-block" data-id="input-email">'.$errors->first('email').'</p>' : "" !!}
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error simple' : '' }}">
            <input type="password" name="password" id="input-password" class="form-control input-lg" value="" placeholder="{{ trans('page/user.field.password') }}">
            {!! $errors->has("password") ? '<p class="help-block" data-id="input-password">'.$errors->first('password').'</p>' : "" !!}
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-primary btn-block btn-lg">@lang('common.buttons.sign_in')</button>
        </div>
    {!! Form::close() !!}
@endsection