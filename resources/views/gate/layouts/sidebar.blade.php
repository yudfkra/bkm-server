<div id="main-menu-inner">
    <ul class="navigation">
        <li class="{{ empty(Request::segment(2)) ? 'active' : '' }}">
            <a href="{{ route('gate.dashboard') }}">
                <i class="menu-icon fa fa-dashboard"></i>
                <span class="mm-text">@lang('layout.menu.dashboard.title')</span>
            </a>
        </li>
         <li class="{{ (Request::segment(2) == 'user') ? 'active' : '' }}">
            <a href="{{ route('gate.user.index') }}">
                <i class="menu-icon fa fa-users"></i>
                <span class="mm-text">@lang('layout.menu.user.title')</span>
            </a>
        </li>
    </ul>
</div>