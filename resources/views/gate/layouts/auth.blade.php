<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8" lang="{{ app()->getLocale() }}"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8" lang="{{ app()->getLocale() }}"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="gt-ie8 gt-ie9 not-ie" lang="{{ app()->getLocale() }}">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ trans_choice('layout.meta.title', !empty($title), ['appname' => config('app.name'), 'title' => $title ?? '']) }}</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/pixel-admin.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/pages.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/themes.min.css') }}">
    <!--[if lt IE 9]>
        <script src="{{ asset('pixel-admin/css/ie.min.js') }}"></script>
    <![endif]-->
    @stack('styles')
</head>
<body class="theme-frost page-signin-alt">
    <div class="signin-header">
        <a href="" class="logo">
            <div class="demo-logo bg-primary"><img src="" alt="" style="margin-top: -4px;"></div>&nbsp;
            <strong>{{ config('app.name') }}</strong>
        </a>
    </div>

    @yield('content')

    <script src="{{ asset('pixel-admin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('pixel-admin/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('pixel-admin/js/pixel-admin.min.js') }}"></script>
    @stack('scripts')
</body>
</html>