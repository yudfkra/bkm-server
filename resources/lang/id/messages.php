<?php

return [
    'errors' => [
        'title' => 'Terjadi kesalahan!',
        'empty_data' => 'Tidak ada data.',
        'not_found' => '{0} Data tidak ditemukan. |{1} :Item tidak ditemukan!',
        'validation' => 'Validasi error!',
        'user' => [
            'failed_create' => 'Gagal menambahkan pengguna, silahkan coba lagi dalam beberapa saat',
            'failed_delete' => 'Gagal menghapus pengguna, silahkan coba lagi dalam beberapa saat',
            'empty_email' => 'Email belum terisi.',
            'empty_password' => 'Password belum terisi.',
            'failed_update' => 'Data pengguna gagal diubah.',
        ],
        'server' => [
            'commant_not_found' => 'Unknown command \':command\'.',
        ]
    ],
    'success' => [
        'title' => 'Sukses!',
        'user' => [
            'created' => "':Name', berhasil ditambahkan ke sistem.",
            'updated' => "Data ':Name' berhasil diubah.",
            'deleted' => "':Name', berhasil dihapus dari sistem.",
        ]
    ],
];