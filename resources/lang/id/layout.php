<?php

return [
    'title' => 'Server',
    'meta' => [
        'title' => '{0} :appname |{1} :title - :appname',
    ],
    'menu' => [
        'login' => [
            'title' => 'Login',
            'description' => 'Masuk untuk memulai sesi anda.'
        ],
        'dashboard' => [
            'title' => 'Dashboard',
            'description' => 'Dashboard',
            'to_app' => 'Masuk ke Aplikasi',
        ],
        'user' => [
            'title' => 'Pengguna',
            'description' => 'Data Pengguna',
        ]
    ],
    'notfound' => [
        'back_or_search' => '<a href=":url">Kembali</a> atau cari data berdasarkan kata kunci.',
    ]
];