<?php

return [
    'field' =>  [
        'keyword' => 'Kata Kunci',
    ],
    'buttons' => [
        'hide_menu' => 'Hide Menu',
        'back' => 'Kembali',
        'save' => 'Simpan',
        'add' => '{0} Tambah |{1} Tambah :Item',
        'detail' => '{0} Detail |{1} Detail :Item',
        'edit' => '{0} Ubah |{1} Ubah :Item',
        'delete' => '{0} Hapus |{1} Hapus :Item',
        'cancel' => '{0} Batal |{1} Batalkan :Item',
        'search' => '{0} Cari |{1} Cari :Item',
        'submit' => 'Submit',
        'sign_in' => 'Masuk',
    ],
    'text' => [
        'action' => 'Aksi',
    ],
];