<?php

return [
    'title' => 'Pengguna',
    'section' => [
        'add' => 'Tambah Pengguna',
        'delete' => 'Hapus Pengguna',
        'edit' => 'Ubah Pengguna',
        'detail' => '{0} Detail Pengguna |{1} :User - Detail Pengguna',
        'filter' => 'Pencarian Data',
    ],
    'field' => [
        'name' => 'Nama',
        'email' => 'Email',
        'password' => 'Password',
        'password_confirmation' => 'Konfirmasi Password',
        'user_type' => 'Tipe Pengguna',
        'status' => [
            'title' => 'Status Pengguna',
            'inactive' => 'Tidak Aktif',
            'active' => 'Aktif',
        ],
        'created_at' => 'Tanggal Ditambahkan',
        'updated_at' => 'Perubahan Terakhir',
    ],
    'text' => [
        'empty_if_not_update' => 'Kosongkan jika tidak ingin diubah.',
    ],
];