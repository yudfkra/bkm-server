<?php

return [
    'errors' => [
        'invalid_checksum' => 'Checksum tidak valid.',
        'empty_broker' => 'Broker tidak terspesifikasi',
        'empty_token' => 'Token tidak terspesifikasi',
        'return_type' => 'Return url tidak terspesifikasi.'
    ]
];