window.initPixel = [];

window.initPixel.push(function () {
    $('.select2').select2();

    $(document).on("focus", ".form-control", function () {
        var input = $(this);
        $('.form-group[data-id="' + input.attr("id") + '"]').removeClass("has-error");
    });

    $(document).on("blur", ".form-control", function () {
        var input = $(this);
        if (input.val()) {
            $('.form-group[data-id="' + input.attr("id") + '"]').removeClass("has-error");
            $('.help-block[data-id="' + input.attr("id") + '"]').hide();
        } else {
            $('.form-group[data-id="' + input.attr("id") + '"]').addClass("has-error");
            $('.help-block[data-id="' + input.attr("id") + '"]').show();
        }
    });

    $('.confirm-modal').on('click', function () {
        const btn = $(this);
        bootbox.confirm({
            message: 'Anda yakin ingin menghapus Data ini ?',
            callback: function (result) {
                if (result) {
                    btn.parent().submit();
                }
            },
            className: "bootbox-sm",
        });
    });
});