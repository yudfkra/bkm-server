<?php

namespace App\Libraries;

use App\Models\Broker;
use App\Models\User;
use App\Models\UserType;
use Jasny\SSO\Server;
use Jasny\ValidationResult;
use Illuminate\Validation\Validator;

class SSOServer extends Server
{
    public function __construct()
    {
        parent::__construct();
    }

    public function logout()
    {
        $this->startBrokerSession();
        $this->setSessionData('sso_user', null);
        return true;
    }

    public function userInfo()
    {
        $this->startBrokerSession();
        $user = null;

        $username = $this->getSessionData('sso_user');

        if ($username) {
            $user = $this->getUserInfo($username);
            if (!$user) return $this->fail("User not found", 500); // Shouldn't happen
        }
        return response()->json($user, 200);
    }

    public function brokerInfo()
    {
        $this->startBrokerSession();
        $broker = $this->getBrokerInfo($this->brokerId);
        return $broker;
    }

    public function attach()
    {
        $request = request();

        $this->detectReturnType();

        if (!$request->filled('broker')) {
            return $this->fail(trans('server.errors.empty_broker'), 400);
        }
        if (!$request->filled('token')) {
            return $this->fail(trans('server.errors.empty_token'), 400);
        }
        if (!$this->returnType) {
            return $this->fail(trans('server.errors.return_type'), 400);
        }

        $checksum = $this->generateAttachChecksum($request->input('broker'), $request->input('token'));

        $reqChecksum = $request->input('checksum');
        if (empty($reqChecksum) || $checksum != $reqChecksum) {
            return $this->fail(trans('server.errors.invalid_checksum'), 400);
        }

        $this->startUserSession();
        $sid = $this->generateSessionId($_REQUEST['broker'], $_REQUEST['token']);

        $this->cache->set($sid, $this->getSessionData('id'));
        return $this->outputAttachSuccess();
    }

    protected function outputAttachSuccess()
    {
        if ($this->returnType === 'image') {
            $this->outputImage();
        }

        if ($this->returnType === 'json') {
            return response()->json(['success' => attached]);
        }

        if ($this->returnType === 'jsonp') {
            return response()->jsonp($_REQUEST['callback'], ['success' => 'attached'], 200);
        }

        if ($this->returnType === 'redirect') {
            // $url = $_REQUEST['return_url'];
            // header("Location: $url", true, 307);
            // echo "You're being redirected to <a href='{$url}'>$url</a>";
            $url = $_REQUEST['return_url']; // request()->input('return_url', null);
            return redirect()->away($url, 307);
        }
    }

    public function authenticate($email, $password)
    {
        if (!isset($email)) {
            return ValidationResult::error(trans('messages.errors.user.empty_email'));
        }
        if (!isset($password)) {
            return ValidationResult::error(trans('messages.error.user.empty_password'));
        }

        $user = User::where('email', $email)->first();
        if (!$user || !$user->checkPassword($password)) {
            return ValidationResult::error(trans('auth.failed'));
        }

        $this->setSessionData('sso_user', $user->id);
        return ValidationResult::success();
    }

    protected function getBrokerInfo($brokerId)
    {
        $broker = Broker::where([
            ['share_id', '=', $brokerId],
            ['status', '=', Broker::BROKER_STATUS['active']]
        ])->first();
        return !empty($broker) ? $broker->toArray() : null;
    }

    protected function getUserInfo($id)
    {
        $user = User::with('type')->find($id);
        return !empty($user) ? $user->toArray() : null;
    }
}
