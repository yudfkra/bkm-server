<?php

namespace App\Http\Controllers\Gate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Broker;

class GateController extends Controller
{
    public function index(Request $request)
    {
        $brokers = Broker::active()->where('share_id', '<>', config('bkm.broker_id'))->get();

        $title = trans('layout.menu.dashboard.title');
        return view('gate.pages.dashboard', compact('brokers', 'title'));
    }
}
