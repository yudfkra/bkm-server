<?php

namespace App\Http\Controllers\Gate;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequestHandle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::where('users.id', '<>', 1)->with(['type'])->latest();

        $offset = (int) $request->input('page', 1);
        $offset = $offset > 1 ? ($offset - 1) * 15 : 0;

        $users->when(
            $request->has('type') || $request->has('status') || $request->has('keyword'),
            function($query) use($request){
                if ($request->filled('type')) {
                    $query->onlyType($request->type);
                }
                if ($request->filled('status')) {
                    $query->where('users.status', $request->status);
                }
                if ($request->filled('keyword')) {
                    $query->whereLikes($request->keyword);
                }
                return $query;
            }
        );

        $params = $request->only(['type', 'status', 'keyword']);

        $data['offset'] = $offset + 1;
        $data['params'] = $params;
        $data['users'] = $users->paginate()->appends($params);

        $data['title'] = trans('layout.menu.user.title');
        // return $data;
        return view('gate.pages.user.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequestHandle $request)
    {
        $messages = [];

        $registered = DB::transaction(function () use($request) {
            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'user_type_id' => $request->input('user_type_id'),
                'status' => $request->input('status'),
            ]);
            return $user;
        });

        if ($registered) {
            $messages['success'][] = trans('messages.success.user.created', ['name' => $registered->name]);
            // send email ?
        }else{
            $messages['errors'][] = trans('messages.errors.user.failed_create');
        }
        $request->session()->flash('flash_messages', $messages);
        
        return $registered 
            ? redirect()->route('gate.user.index') 
            : redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('type')->find($id);

        $title = trans_choice('page/user.section.detail', !empty($user), [
            'user' => $user ? $user->name : null,
        ]);
        return view('gate.pages.user.detail', compact('user', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequestHandle $request, $id)
    {
        $user =  User::with('type')->find($id);
        if (!empty($user)) {
            $messages = [];

            $updated = DB::transaction(function () use ($user, $request) {
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->user_type_id = $request->input('user_type_id');
                $user->status = $request->input('status');

                if ($request->filled('password')) {
                    $user->password = bcrypt($request->input('password'));
                }
                return $user->save();
            });

            if ($updated) {
                $messages['success'][] = trans('messages.success.user.updated', ['name' => $user->name]);
            } else {
                $messages['errors'][] = trans('messages.errors.user.failed_update');
            }
            $request->session()->flash('flash_messages', $messages);

            return $updated
                ? redirect()->route('gate.user.show', ['id' => $user->id])
                : redirect()->back()->withInput();
        }else{
            $request->session()->flash('flash_messages', ['errors' => trans('messages.errors.user.notfound')]);
            return redirect()->route('gate.user.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::with('type')->find($id);
        if (!empty($user)) {
            // todo check jika user menghapus dirinya sendiri dan tidak bisa menghapus tipe system admin ?
            $deleted = DB::transaction(function () use ($user) {
                return $user->delete();
            });

            $messages = [];
            if ($deleted) {
                $request->session()->flash('flash_messages', [
                    'success' => trans('messages.success.user.deleted', ['name' => $user->name]),
                ]);
            } else {
                $request->session()->flash('flash_messages', ['errors' => trans('messages.errors.user.failed_delete')]);
            }
        }else{
            $request->session()->flash('flash_messages', ['errors' => trans('messages.errors.user.notfound')]);
        }
        return redirect()->route('gate.user.index');
    }
}
