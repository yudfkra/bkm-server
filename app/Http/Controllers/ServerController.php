<?php

namespace App\Http\Controllers;

use App\Libraries\SSOServer;
use Illuminate\Http\Request;

class ServerController extends Controller
{
    /**
     * Undocumented variable
     *
     * @var App\Libraries\SSOServer
     */
    protected $server;

    /**
     * Undocumented function
     *
     * @param SSOServer $server
     * @return void
     */
    public function __construct(SSOServer $server)
    {
        $this->server = $server;
    }

    /**
     * Handle request from brokers
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $command = $request->input('command', null);
        if (!$command || !method_exists($this->server, $command)) {
            return response()->json([
                'error' => trans('messages.errors.server.commant_not_found',['command' => $command]),
            ], 404);
        }
        $result = $this->server->$command();
        return $result;
    }

    public function showLoginForm(Request $request)
    {
        $this->server->startBrokerSession();

        $data['params'] = $request->only(['access_token', 'follow']);
        $data['title'] = trans('layout.menu.login.title');
        return view('gate.pages.login', $data);
    }

    public function login(Request $request)
    {
        $this->server->startBrokerSession();

        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $attemped = $this->server->authenticate($request->input('email'), $request->input('password'));
        if ($attemped->failed()) {
            $request->session()->flash('flash_messages', [
                'errors' => $attemped->getError(),
            ]);
            return redirect()->back()->withInput();
        }
        // set session

        if ($request->filled('follow')) {
            $broker = $this->server->brokerInfo();
            return redirect()->away($broker['url'], 302);
        }
    }

    public function logout()
    {
        $this->server->logout();
        return redirect()->route('server.login');
    }
}
