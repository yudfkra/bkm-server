<?php

namespace App\Http\ViewComposers;

use App\Models\UserType;
use App\Models\User;

class UserViewComposer
{
    public function compose($view)
    {
        $view->with([
            'user_types' => $this->getUserType(),
            'user_status' => $this->getUserStatus(),
        ]);
    }

    public function getUserType()
    {
        return UserType::pluck('type', 'id');
    }

    public function getUserStatus()
    {
        $status = [];
        foreach (User::USER_STATUS as $key => $det) {
            $status[$det] = trans('page/user.field.status.' . $key);
        }
        return $status;
    }
}
