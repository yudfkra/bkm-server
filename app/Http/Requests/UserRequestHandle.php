<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequestHandle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'user_type_id' => 'required|exists:user_types,id',
            'status' => 'required|in:' . implode(',' , array_values(\App\Models\User::USER_STATUS))
        ];
        switch ($this->method()) {
            case 'PUT':
            case 'PATCH':
                $rules['email'] = 'required|email|unique:users,email,' . $this->route()->parameter('user') . ',id';
                $rules['password'] = 'nullable|string|min:6|confirmed';
                break;
            default:
                $rules['email'] = 'required|email|unique:users,email';
                $rules['password'] = 'required|string|min:6';
                break;
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => trans('page/user.field.name'),
            'user_type_id' => trans('page/user.field.user_type'),
            'status' => trans('page/user.field.status.title'),
            'email' => trans('page/user.field.email'),
            'password' => trans('page/user.field.password'),
        ];
    }
}
