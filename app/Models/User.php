<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type_id', 'name', 'email', 'password', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const PAGINATION_LIMIT = 15;

    const USER_STATUS = [
        'active' => 1,
        'inactive' => 0,
    ];

    public function getUserStatusDisplayAttribute()
    {
        return array_flip(self::USER_STATUS)[$this->attributes['status']];
    }

    public function scopeOnlyType($query, $types)
    {
        $_types = !is_array($types) ? [$types] : $types;
        $query->whereIn('users.user_type_id', $_types);
        return $query;
    }

    public function scopeWhereLikes($query, $keyword)
    {
        $query->where(function($q) use($keyword){
            $q->where('users.name', 'like', "%{$keyword}%")
                ->orWhere('users.email', 'like', "%{$keyword}%");
        });
        return $query;
    }

    public function checkPassword($password)
    {
        return Hash::check($password, $this->password);
    }

    public function type()
    {
        return $this->belongsTo(UserType::class, 'user_type_id', 'id');
    }
}
