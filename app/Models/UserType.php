<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    protected $table = 'user_types';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'type',
    ];

    public $timestamps = false;

    const USER_TYPES = [
        'admin' => 1,
        'sales' => 2,
        'cs' => 3,
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'user_type_id', 'id');
    }

    public function getLabelTypeDisplayAttribute()
    {
        switch ($this->attributes['id']) {
            case static::USER_TYPES['admin']:
                $label = 'info';
                break;
            case static::USER_TYPES['sales']:
                $label = 'primary';
                break;
            default:
                $label = 'warning';
                break;
        }
        return $label;
    }
}
