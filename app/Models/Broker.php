<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Broker extends Model
{
    protected $table = 'brokers';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'share_id', 'secret', 'url', 'status',
    ];

    const BROKER_STATUS = [
        'inactive' => 0,
        'active' => 1,
    ];

    public function scopeActive($query)
    {
        return $query->where('status', static::BROKER_STATUS['active']);
    }
}
