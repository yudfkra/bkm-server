<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Broker::class, function (Faker $faker) {
    $company = $faker->company;
    return [
        'name' => $company,
        'share_id' => $faker->randomNumber(7),
        'secret' => sha1($company . 'x' . env('APP_NAME')),
        'url' => $faker->url,
        'status' => $faker->numberBetween(0, 1),
    ];
});
