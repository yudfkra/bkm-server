<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class)->states('system-admin')->create([
            'name' => 'Execution Last',
            'email' => 'thelastexecution@gmail.com',
            'password' => bcrypt('yudayuda'),
            'status' => true,
        ]);

        factory(App\Models\User::class, rand(1, 20))->states('sales-admin')->create();
        factory(App\Models\User::class, rand(1, 20))->states('cs')->create();
    }
}
