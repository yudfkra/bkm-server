<?php

use Illuminate\Database\Seeder;

class BrokerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Broker::class)->create([
            'name' => 'BKM Administrator',
            'url' => 'http://localhost:8080/gate',
            'status' => App\Models\Broker::BROKER_STATUS['active'],
        ]);

        factory(App\Models\Broker::class)->create([
            'name' => 'BKM Sales',
            'url' => 'http://localhost:9000',
            'status' => App\Models\Broker::BROKER_STATUS['active'],
        ]);

        factory(App\Models\Broker::class)->create([
            'name' => 'BKM Messenger',
            'url' => 'http://localhost:9090',
            'status' => App\Models\Broker::BROKER_STATUS['active'],
        ]);
    }
}
